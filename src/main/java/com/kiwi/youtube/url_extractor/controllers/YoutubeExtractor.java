package com.kiwi.youtube.url_extractor.controllers;

import com.kiwi.youtube.url_extractor.extractor.YouTubeExtractor;
import com.sun.istack.internal.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotEmpty;


@RestController
public class YoutubeExtractor {
    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/get-youtube-url")
    public @ResponseBody
    String getYoutubeUrl(
            @RequestParam @NotNull @NotEmpty String url,
            HttpServletRequest request) throws Exception {
        YouTubeExtractor youTubeExtractor = new YouTubeExtractor();
        String extractedUrl =  youTubeExtractor.extract(url);
        return extractedUrl;
    }

}
