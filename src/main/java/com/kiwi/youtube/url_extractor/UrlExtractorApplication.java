package com.kiwi.youtube.url_extractor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrlExtractorApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrlExtractorApplication.class, args);
	}

}
